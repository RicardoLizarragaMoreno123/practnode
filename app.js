const express = require('express');
const http =require ('http');
const puerto = 3000;
const json = require ('body-parser')
const app = express();

//asignaciones
app.use("view engine", "ejs");
app.use(json.urlencoded({extends:true}))

//asignar al objeto inf
app.set(express.static(__dirname + '/public'));

app.get('/',(req,res)=>{
    res.render('index', {titulo: "Mi primer página ejs", nombre: "Alexis Tirado"});

});
app.get('/tabla',(req,res)=>{

    const params = {
        numero: req.query.numero

    }
    res.render('tabla',params);

})
app.post('/tabla',(req,res)=>{

    const params = {
        numero : req.body.numero
    }
    res.render('tabla',params);
})

app.listen(puerto,()=>{
    console.log("Iniciando el servidor" + puerto);
});
